using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    private InputController inputController;
    private Rigidbody playerRigidBody;
    private bool boosted = false;
    [SerializeField]
    private float speed;
    private void Awake()
    {
        inputController = new InputController();
        playerRigidBody = GetComponent<Rigidbody>();
        inputController.Kart.Boost.performed += context => Boost();
    }
    private void OnEnable()
    {
        inputController.Enable();
    }
    private void OnDisable()
    {
        inputController.Disable();
    }
    private void FixedUpdate() {
        Vector2 moveDirect = inputController.Kart.Move.ReadValue<Vector2>();
        Debug.Log(moveDirect);
        Move(moveDirect);
    }
    // Update is called once per frame
    private void Move(Vector2 direction)
    {
        playerRigidBody.velocity = new Vector3(direction.x * speed + Time.fixedDeltaTime, 0f, direction.y * speed + Time.fixedDeltaTime);
    }
    void Boost()
    {
        if (!boosted)
        {
            speed = speed * 2;
            boosted = true;
        }
        else
        {
            speed = speed / 2;
            boosted = false;
        }
    }
}
